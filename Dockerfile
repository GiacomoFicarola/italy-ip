# Select distro
FROM debian:stable-slim

# Install packets and clean
RUN apt-get update && \
        apt-get install -y unzip wget python3 python3-pip git && \
        apt-get autoremove -y && apt-get autoclean -y && apt-get clean -y

# Exec the core of the sys
RUN git clone https://gitlab.com/GiacomoFicarola/ip2location-python-csv-converter.git && \
        cd ip2location-python-csv-converter && \
        python3 setup.py install && \
        cd ..
